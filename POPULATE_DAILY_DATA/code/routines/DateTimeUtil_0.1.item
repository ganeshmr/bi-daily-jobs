package routines;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

/*
 * user specification: the function's comment should contain keys as follows: 1. write about the function's comment.but
 * it must be before the "{talendTypes}" key.
 * 
 * 2. {talendTypes} 's value must be talend Type, it is required . its value should be one of: String, char | Character,
 * long | Long, int | Integer, boolean | Boolean, byte | Byte, Date, double | Double, float | Float, Object, short |
 * Short
 * 
 * 3. {Category} define a category for the Function. it is required. its value is user-defined .
 * 
 * 4. {param} 's format is: {param} <type>[(<default value or closed list values>)] <name>[ : <comment>]
 * 
 * <type> 's value should be one of: string, int, list, double, object, boolean, long, char, date. <name>'s value is the
 * Function's parameter name. the {param} is optional. so if you the Function without the parameters. the {param} don't
 * added. you can have many parameters for the Function.
 * 
 * 5. {example} gives a example for the Function. it is optional.
 */
public class DateTimeUtil {
	private static final String TIMEZONE_ID_UTC = "UTC";

	public static Date getUTCDate() {
		TimeZone.setDefault(TimeZone.getTimeZone(TIMEZONE_ID_UTC));
		Calendar cal = Calendar.getInstance(TimeZone
				.getTimeZone(TIMEZONE_ID_UTC));
		return cal.getTime();
	}

	public static DateTime getUTCDateTime() {
		DateTime nowDateTime = new DateTime(DateTimeZone.UTC)
				.withMillisOfSecond(0);
		return nowDateTime;
	}

	public static String getUTCNowStr(DateTime dt) {
		return dt.toString();
	}

	public static Date getDate(DateTime dt) {
		if (dt == null) {
			return null;
		}
		return dt.toDate();
	}

	public static Date addMonths(Date dt, int addMonths) {
		if (dt == null || addMonths == 0) {
			return dt;
		}
		Calendar c = Calendar.getInstance();
		c.setTime(dt);
		c.add(Calendar.MONTH, addMonths);
		return c.getTime();
	}

	public static DateTime isoDateToDateTime(String isoDate) {
		DateTime date = null;
		if (isoDate != null && !"".equals(isoDate)) {
			DateTimeFormatter isoDateFormatter = ISODateTimeFormat
					.dateTimeNoMillis();
			try {
				date = isoDateFormatter.parseDateTime(isoDate);
			} catch (Throwable t) {
			}
		}
		return date;
	}

	public static String getDateKey(DateTime dateTime) {
		long dateKey = dateTime.getYear() * 10000 + dateTime.getMonthOfYear()
				* 100 + dateTime.getDayOfMonth();
		return "" + dateKey;
	}

	public static String getTimeKey(DateTime dateTime) {
		int hourDayInt = dateTime.getHourOfDay();
		String hourDay = "" + hourDayInt;
		if (hourDayInt < 10) {
			hourDay = "0" + hourDay;
		}

		int minOfHrInt = dateTime.getMinuteOfHour();
		String minOfHr = "" + minOfHrInt;
		if (minOfHrInt < 10) {
			minOfHr = "0" + minOfHr;
		}

		return hourDay + ":" + minOfHr;
	}

	public static String getDateAndTimeKey(String timeStr) {
		DateTime dateTime = isoDateToDateTime(timeStr);
		if (dateTime == null) {
			return "";
		}
		return getDateKey(dateTime) + "," + getTimeKey(dateTime);
	}

	public static boolean isNewerPlan(int curPlanStartDateKey,
			String curPlanStartTimeKey, Date planStartDate) {
		if (planStartDate == null || curPlanStartDateKey == 0) {
			return false;
		}

		int newDateKey = 0;
		try {
			newDateKey = Integer.parseInt(TalendDate.formatDate("yyyyMMdd",
					planStartDate));
		} catch (Exception e) {
			return false;
		}

		if (newDateKey > curPlanStartDateKey) {
			return true;
		} else if (newDateKey == curPlanStartDateKey) {
			String newTimeKey = TalendDate.formatDate("HH:mm", planStartDate);
			boolean olderPlanEmpty = curPlanStartTimeKey == null
					|| "".equals(curPlanStartTimeKey);
			if (!olderPlanEmpty) {
				return (curPlanStartTimeKey.compareToIgnoreCase(newTimeKey) < 0);
			} else {
				return true;
			}
		}
		return false;
	}
}
